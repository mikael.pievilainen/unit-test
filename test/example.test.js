const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        //initialization
        // create objects... etc...
        console.log("Initialising tests")
    });
    it("Can add 1 and 2 together", () => {
        // test
        expect(mylib.add(1, 2)).equal(3), "1 + 2 is not 3, for some reason?";
    });
    it("ERROR", () => {
        // test 2.0
        expect(mylib.divide(8,0)).throw
    });
    after("", () => {
        // cleanup
        // For example: shut express server down
        console.log("Testing completed!")
    });
});